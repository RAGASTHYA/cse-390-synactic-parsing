package cse_390_assignment_3;

import java.util.HashMap;

/**
 *
 * @author Rahul S Agasthya and Varun Goel
 *
 * CSE 390 - Special Topics in Computer Science - Natural Language Processing
 * Homework 3 - Syntactic Parsing
 *
 * Class to split the training data and the tags.
 */
public class ProcessingMethods {

    /**
     * Removes words.
     *
     * @param testString
     * @return String with tags.
     */
    public static String removeWords(String testString) {
        return testString.replaceAll("[(]{1}\\w+[)]{1}", "").replaceAll("[(]{1}\\W+[)]{1}", "").replaceAll("[(]{1}\\w{1}.\\w{1}[)]{1}", "").replace("PUNC", ")").replace("'", "H").replace("_", "U");
    }

    /**
     * Prepares the input string for tokenization
     *
     * @param testString
     * @return Tokenized String
     */
    public static String prepareForTokenizer(String testString) {
        String replacement = "";
        if (testString.contains("PUNC(?)")) {
            replacement = "QUESTIONMARK";
        } else if (testString.contains("PUNC(.)")) {
            replacement = "FULLSTOP";
        }

		//testString.replaceAll("PUNC[(]\\W[)]","PUNC(" + replacement + ")");
        return testString.replaceAll("PUNC[(]\\W[)]", "PUNC(" + replacement + ")").replace("'", "H").replace("_", "U");
    }

    /**
     * Helper method to place a word in the map
     *
     * @param toPut
     * @param map
     */
    public static void putStringInMap(String toPut, HashMap<String, Integer> map) {
        if (map.get(toPut) != null) {
            map.put(toPut, map.get(toPut) + 1);
        } else {
            map.put(toPut, 1);
        }
    }

    /**
     * Calculates the laplace probabiity
     *
     * @param key is the whole grammar rule
     * @param leftSide is the S in S -> A B
     * @param grammarRulesMap is the map with all the grammar rules
     * @param nonTerminalsMap
     * @return
     */
    public static double calculateLaplaceProbability(String key, String leftSide, HashMap<String, Integer> grammarRulesMap, HashMap<String, Integer> nonTerminalsMap) {
        double numerator = 1;
        double denominator = nonTerminalsMap.size();

        if (nonTerminalsMap.get(leftSide) != null && grammarRulesMap.get(key) != null) {
            numerator += grammarRulesMap.get(key);
            denominator += nonTerminalsMap.get(leftSide);
        }

        return numerator / denominator;
    }
}
