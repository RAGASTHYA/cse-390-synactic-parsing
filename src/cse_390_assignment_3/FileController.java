package cse_390_assignment_3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
/**
 * 
 * @author Rahul S Agasthya and Varun Goel
 * 
 * CSE 390 - Special Topics in Computer Science - Natural Language Processing
 * Homework 3 - Syntactic Parsing
 * 
 * Class to Save and Load the Json file comprising the grammar for the CYK Parser.
 */
public class FileController {

    /**
     * The following variables are used for saving the data of a HashMap into a 
     * JSON File.
     */
    public static String ROOT_NODE = "GRAMMAR DATA";
    public static String PARENT_NODE = "Parent";
    public static String CHILD_A = "ChildA";
    public static String CHILD_B = "ChildB";
    public static String LAPLACE_PROB = "LaplaceProbability";
    public static String JSON_EXT = ".json";

    //The HashMap that shall be used in this class.
    public static HashMap<String, Double> dataMap;

    /**
     * Default No arg constructor, that is mainly used, if dataMap == null.
     */
    public FileController() {
        dataMap = new HashMap<String, Double>();
    }

    /**
     * Constructor, where the HashMap is sent in as a Parameter.
     * @param initDataMap 
     */
    public FileController(HashMap<String, Double> initDataMap) {
        dataMap = initDataMap;
    }

    /**
     * Function to save the HashMap dataMap into a JSON File named "title.json".
     * @param title the name of the JSON File.
     * @throws FileNotFoundException 
     */
    public  void saveJSON(String title) throws FileNotFoundException {
    	

		Map<String, Object> properties = new HashMap<>(1);
		properties.put(JsonGenerator.PRETTY_PRINTING, true);
		JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
    	
        String jsonFilePath = title + JSON_EXT;

        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = writerFactory.createWriter(os);

        JsonArray grammar = makeDataJsonArray();

        JsonObject courseJsonObject = Json.createObjectBuilder()
                .add(ROOT_NODE, grammar)
                .build();

        jsonWriter.writeObject(courseJsonObject);
    }

    /**
     * Makes a JsonArray out of the dataMap HashMap, which will be saved in the
     * saveJson() metod.
     * This method also splits the key into the parent and two children, then
     * creates JsonObject by calling the makeJsonObjects() method, and finally
     * adding the JsonObject to the JsonArray.
     * @return JsonArray - The HashMap where the contents are stored.
     */
    public static JsonArray makeDataJsonArray() {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        Iterator<Map.Entry<String, Double>> it = dataMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Double> entry = it.next();
            String[] splitEntry = entry.getKey().split(":");

            String parent = splitEntry[0];
            String children = "$ $";
            if (splitEntry.length > 1) {
                children = splitEntry[1];
            }

            String[] childArray = children.split("\\s");
            String childA = "$";
            String childB = "$";
            if (childArray.length == 1) {
                childA = childArray[0];
            } else if (childArray.length == 2) {
                childA = childArray[0];
                childB = childArray[1];
            }

            double probability = entry.getValue();

            arrayBuilder.add(makeJsonObjects(parent, childA, childB, probability));
        }
        JsonArray jA = arrayBuilder.build();

        return jA;
    }

    /**
     * This method accepts the parameters and returns a JsonObject, which will 
     * be added to the JsonArray in the makeDataJsonArray() method.
     * @param parent This is the parent node.
     * @param childA This is the first child of the parent node. '$' is stored
     *                  if the node is null.
     * @param childB This is the second child of the parent node. '$' is stored
     *                  if the node is null.
     * @param probability The Laplace Smoothed Probability of the data set.
     * @return JsonObject - This is added to the Json array.
     */
    public static JsonObject makeJsonObjects(String parent, String childA, String childB, double probability) {
        JsonObject jso = Json.createObjectBuilder()
                .add(PARENT_NODE, parent)
                .add(CHILD_A, childA)
                .add(CHILD_B, childB)
                .add(LAPLACE_PROB, String.valueOf(probability))
                .build();
        return jso;
    }

    public  void loadJSON(String fileName) throws FileNotFoundException, IOException {
        JsonObject json = loadJSONFile(fileName);
        //dataMap.clear();
        
        JsonArray jsonArrayObject = json.getJsonArray(ROOT_NODE);
        
        for(int i=0; i< jsonArrayObject.size(); i++) {
            JsonObject jso = jsonArrayObject.getJsonObject(i);
            String key = jso.getString(PARENT_NODE) + ":" + (jso.getString(CHILD_A).equals("$")?"":jso.getString(CHILD_A)) + (jso.getString(CHILD_B).equals("$")?"":jso.getString(CHILD_B));
            double value = Double.parseDouble(jso.getString(LAPLACE_PROB));
            dataMap.put(key, value);            
        }
    }

    public static JsonObject loadJSONFile(String fileName) throws FileNotFoundException, IOException {
        String jsonFilePath = fileName + JSON_EXT;
        InputStream load = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(load);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        load.close();
        return json;
    }

    /**
     * Getter to get the data stored in the HashMap dataMap.
     * @return dataMap
     */
    public  HashMap<String, Double> getDataMap() {
        return dataMap;
    }

    /**
     * Setter to change the value of the HashMap dataMap.
     * @param initDataMap The new value of the HashMap. 
     */
    public static void setDataMap(HashMap<String, Double> initDataMap) {
        dataMap = initDataMap;
    }
}