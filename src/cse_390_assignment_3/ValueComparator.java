package cse_390_assignment_3;

import java.util.Comparator;
import java.util.Map;


/**
 * 
 * @author Rahul S Agasthya and Varun Goel
 * 
 * CSE 390 - Special Topics in Computer Science - Natural Language Processing
 * Homework 3 - Syntactic Parsing
 * 
 * Implementing the comparator class to order the hashmap
 */
public class ValueComparator implements Comparator<String> {
	Map base;

	public ValueComparator(Map base) {
		this.base = base;
	}

	// Note: this comparator imposes orderings that are inconsistent with
	// equals.

	public int compare(String a, String b) {
		// returning 0 would merge keys
		Integer first = (Integer)base.get(a);
		Integer second = (Integer)base.get(b);
		
		if(first.compareTo(second) == 0)
			return 1;
		else
			return -first.compareTo(second);		
	}
}
