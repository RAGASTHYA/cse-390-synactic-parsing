/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cse_390_assignment_3;

/**
 * 
 * @author Rahul S Agasthya and Varun Goel
 * 
 * CSE 390 - Special Topics in Computer Science - Natural Language Processing
 * Homework 3 - Syntactic Parsing
 * 
 * Triple class for the CYKParser class.
 */
public class Triple {
	public String parent;
	public String leftChild;
	public String rightChild;
	
	/**
	 * Constructor which will represent a rule with only one child
	 * @param parent
	 * @param leftChild
	 */
	public Triple(String parent, String leftChild){
		this.parent = parent;
		this.leftChild = leftChild;
		rightChild = null;
	}
	
	/**
	 * Constructor which will represent a rule with two child
	 * @param parent
	 * @param leftChild
	 * @param rightChild
	 */
	public Triple(String parent, String leftChild, String rightChild){
		this.parent = parent;
		this.leftChild = leftChild;
		this.rightChild = rightChild;
	}
	
	public boolean isUnary(){
		return rightChild.equals(null);
	}
        
        public String toString(){
            return parent + "->" + leftChild + " " + rightChild;
        }
}
