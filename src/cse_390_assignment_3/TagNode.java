package cse_390_assignment_3;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
/**
 *
 * @author Rahul S Agasthya and Varun Goel
 *
 * CSE 390 - Special Topics in Computer Science - Natural Language Processing
 * Homework 3 - Syntactic Parsing
 *
 * Tag Node class for the tree.
 */
public class TagNode {
	public String name;
	public ArrayList<TagNode> children = new ArrayList<TagNode>();

        /**
         * Constructor with tokenizer argument
         * @param tokenizer
         * @throws IOException 
         */
	public TagNode(StreamTokenizer tokenizer) throws IOException  {
		name = tokenizer.sval; //read the name
		// read the name and check for the children
		if (tokenizer.nextToken() == '(') {     
			tokenizer.nextToken();                
			do {
				//keep on parsing till a ) is hit
				// Add and parse a child
				children.add(new TagNode(tokenizer));  
			} while (tokenizer.ttype != ')');     
			tokenizer.nextToken();               
		}
	}

        /**
         * Parse the parenthesized expression to create the parse tree
         * @param s
         * @return TagNode
         * @throws IOException 
         */
	public static TagNode parseTree(String s) throws IOException {
		StreamTokenizer tokenizer = new StreamTokenizer(new StringReader(s));
		tokenizer.nextToken();                 // Move to first token
		TagNode result = new TagNode(tokenizer);     // Parse root node (and children)
		if (tokenizer.ttype != StreamTokenizer.TT_EOF) {
			System.out.println("Invalid sequence!");
		}
		return result;
	}

        /**
         * Prints the tree in a pre-order style
         * @param startNode
         * @param indents
         * @param grammarRules
         * @param nonTerminals 
         */
	public static void preOrder(TagNode startNode,int indents,ArrayList<String> grammarRules, ArrayList<String> nonTerminals){
		for(int i = 0; i < indents; i++){
			System.out.print("    ");
		}

		if(startNode.name.equals("FULLSTOP"))
			startNode.name = ".";
		else if(startNode.name.equals("QUESTIONMARK"))
			startNode.name = "?";


		//The "_" were replaced with "U" for easier parsing. The "'" were replaced with "H". Re-replace them.
		if(!startNode.name.equals("PUNC"))
			startNode.name = startNode.name.replaceAll("U", "_").replaceAll("H", "'");
		else
			startNode.name = "PUNC";
		//System.out.println(startNode.name);
		//System.out.println();
		
		if(startNode.children.size() > 0)
			nonTerminals.add(startNode.name);

		if(startNode.children.size() == 2){
			String nameToAdd  = "";

			if(startNode.children.get(1).toString().equals("P_NC")){
				nameToAdd = "PUNC";
			}
			else{
				nameToAdd = startNode.children.get(1).toString();
			}
			grammarRules.add(startNode.toString() + ":" + startNode.children.get(0).toString() + " " + nameToAdd);
		}
		else if(startNode.children.size() == 1){
			String nameToAdd  = "";

			if(startNode.toString().equals("P_NC")){
				nameToAdd = "PUNC";
			}
			else{
				nameToAdd = startNode.toString();
			}

			grammarRules.add(nameToAdd + ":" + startNode.children.get(0).toString());
		}

		if(startNode.children.size() > 0){
			preOrder(startNode.children.get(0), indents + 1, grammarRules, nonTerminals);
		}
		if(startNode.children.size() > 1){
			preOrder(startNode.children.get(1), indents + 1, grammarRules, nonTerminals);
		}
	}

        /**
         * Prints the tree in a pre-order style
         * @param indents 
         */
	public void preOrder(int indents){
		for(int i = 0; i < indents; i++){
			System.out.print("    ");
		}
		//The "_" were replaced with "U" for easier parsing. The "'" were replaced with "H". Re-replace them.
		this.name = name.replaceAll("U", "_").replaceAll("H", "'");
		System.out.println(this.name);


		if(this.children.size() > 0){
			this.children.get(0).preOrder(indents + 1);
		}
		if(this.children.size() > 1){
			this.children.get(1).preOrder(indents + 1);
		}
	}

        /**
         * Method to print the tags.
         * @return String
         */
	public String toString(){
		if(this.name.equals("FULLSTOP"))
			return ".";
		else if(this.name.equals("QUESTIONMARK"))
			return "?";
		else if(this.name.contains("P_NC"))
			return "PUNC";
		return this.name.replaceAll("U", "_").replaceAll("H", "'");
	}
}
