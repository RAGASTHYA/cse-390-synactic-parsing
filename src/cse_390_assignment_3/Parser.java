package cse_390_assignment_3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
/**
 *
 * @author Rahul S Agasthya and Varun Goel
 *
 * CSE 390 - Special Topics in Computer Science - Natural Language Processing
 * Homework 3 - Syntactic Parsing
 *
 * Main class to run the project.
 * This contains the main method.
 */
public class Parser {
    /**
     * The main function to run the project.
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {

        //This will have all the grammar rules with their counts
        HashMap<String, Integer> grammarRulesMap = new HashMap<String, Integer>();
        //This will have all the non terminals with their frequency 
        HashMap<String, Integer> nonTerminalsMap = new HashMap<String, Integer>();

        HashMap<String, Double> probabilityMap = new HashMap<String, Double>();

        ArrayList<String> rules = new ArrayList<String>();
        //This will have all the LHS that we see (will include repetitions)
        ArrayList<String> nonTerminals = new ArrayList<String>();

        //This will have all the grammar rules
        ArrayList<String> grammar = new ArrayList<String>();

        HashMap<String, Integer> nonterms = new HashMap<String, Integer>();

        File trainFile = new File("traintrees.txt");
        Scanner input = new Scanner(trainFile);

        String sentence = "";

        while (input.hasNextLine()) {
            sentence = input.nextLine();
            sentence = ProcessingMethods.prepareForTokenizer(sentence);
            TagNode myTagNode = TagNode.parseTree(sentence);
            TagNode.preOrder(myTagNode, 0, rules, nonTerminals);
            //i++;
        }

        input.close();

        //building the map for the grammar rules. Sample Grammar: S -> A B		
        for (int j = 0; j < rules.size(); j++) {
            ProcessingMethods.putStringInMap(rules.get(j), grammarRulesMap);
        }
        //building the map for the symbols on the left side of the rule
        for (int j = 0; j < nonTerminals.size(); j++) {
            ProcessingMethods.putStringInMap(nonTerminals.get(j), nonTerminalsMap);
        }

        //building the probability map
        for (String key : grammarRulesMap.keySet()) {
            String leftSide = key.split(":")[0];
            //System.out.println(leftSide + "		" + key);
            double laplaceProbability = ProcessingMethods.calculateLaplaceProbability(key, leftSide, grammarRulesMap, nonTerminalsMap);
            probabilityMap.put(key, laplaceProbability);
        }

        int count = 0;
        for (String key : nonTerminalsMap.keySet()) {
            nonterms.put(key, count++);
        }

        //building the grammar list
        for (String key : probabilityMap.keySet()) {
            grammar.add(key);
        }

        System.out.println();
        System.out.println(grammarRulesMap);
        System.out.println(probabilityMap);

        System.out.println("NONTERMS: " + nonterms);

        //THIS IS A TESTER FOR THE SAVE AND LOAD.
        FileController file = new FileController(probabilityMap);
        file.saveJSON("Grammar");

        //sorting the bigrams MLE hashmap
        ValueComparator bvc = new ValueComparator(grammarRulesMap);
        TreeMap<String, Integer> sorted_map = new TreeMap<String, Integer>(bvc);
        sorted_map.putAll(grammarRulesMap);

        //the set of keys in the sorted bigrams name hashmap
        Set<String> setOfMLEKeys = sorted_map.keySet();
        //the iterator for the set of keys
        Iterator<String> iterator = setOfMLEKeys.iterator();
        System.out.println("\n\n");
        while (iterator.hasNext()) {
            String key = iterator.next();
            System.out.printf("%-20s%-20s\n", key, grammarRulesMap.get(key));
        }

        File testFile = new File("test.txt");
        Scanner test = new Scanner(testFile);

        while (test.hasNextLine()) {
            String[] words = test.nextLine().split("\\s");
            CYKParser cyk = new CYKParser();
            cyk.CYK(words, nonterms, probabilityMap, nonTerminals.size());
        }
    }

}
