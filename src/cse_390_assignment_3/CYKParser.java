package cse_390_assignment_3;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.HashMap;

/**
 * 
 * @author Rahul S Agasthya and Varun Goel
 * 
 * CSE 390 - Special Topics in Computer Science - Natural Language Processing
 * Homework 3 - Syntactic Parsing
 * 
 * PCFG Parser that implements CYK Algorithm.
 */
public class CYKParser {

    /**
     * This implements the CYK Algorithm with the following parameters.
     * @param words
     * @param nonterms
     * @param grammar
     * @param nonTermsSize 
     */
    public void CYK(String[] words, HashMap<String, Integer> nonterms, HashMap<String, Double> grammar, int nonTermsSize) {
        System.out.println();
        System.out.println(nonterms);
        double[][][] score = new double[words.length + 1][words.length + 1][nonTermsSize];
        Triple[][][] back = new Triple[words.length + 1][words.length + 1][nonTermsSize];

        for (int i = 0; i < words.length; i++) {
            int begin = i;
            int end = i + 1;
            int count = 0;
            for (String A : nonterms.keySet()) {
                if (grammar.get(A + ":" + words[begin]) != null) {
                    score[begin][end][nonterms.get(A)] = grammar.get(A + ":" + words[begin]);
                    count++;
                }
                addUnary(count, score, back, nonterms, grammar, begin, end);
            }
        }

        for (int span = 2; span < words.length; span++) {
            int count = 0;
            for (int begin = 0; begin < words.length - span; begin++) {
                int end = begin + span;
                for (int split = begin + 1; split < end - 1; split++) {
                    for (String A : nonterms.keySet()) {
                        for (String B : nonterms.keySet()) {
                            for (String C : nonterms.keySet()) {
                                if(grammar.get(A + ":" + B + " " + C) != null){
                                    count++;
                                    double prob = Math.log(score[begin][split][nonterms.get(B)])+Math.log(score[split][end][nonterms.get(C)]) + Math.log(grammar.get(A + ":" + B + " " + C));
                                    if(prob > score[begin][end][nonterms.get(A)]) {
                                        score[begin][end][nonterms.get(A)] = prob;
                                        back[begin][end][nonterms.get(A)] = new Triple(A, B, C);
                                    }
                                }   
                                addUnary(count, score, back, nonterms, grammar, begin, end);
                            }
                        }
                    }
                }
            }
        }
        buildTree(score, back);
    }

    /**
     * addUnary() Method that is used in the above method.
     * @param count
     * @param score
     * @param back
     * @param nonterms
     * @param grammar
     * @param begin
     * @param end 
     */
    public void addUnary(int count, double[][][] score, Triple[][][] back, HashMap<String, Integer> nonterms, HashMap<String, Double> grammar, int begin, int end) {
        int i = 0;
        while (i < count) {
            for (String A : nonterms.keySet()) {
                for (String B : nonterms.keySet()) {
                    if (grammar.get(A + " : " + B) != null) {
                        double prob = Math.log(grammar.get(A + " : " + B)) + Math.log(score[begin][end][nonterms.get(B)]);
                        if (prob > score[begin][end][nonterms.get(A)]) {
                            score[begin][end][nonterms.get(A)] = prob;
                            back[begin][end][nonterms.get(A)] = new Triple(A, B);
                        }
                    }
                }
                i++;
            }
        }
    }

    /**
     * Prints the backpointers and scores.
     * @param score
     * @param back 
     */
    public void buildTree(double[][][] score, Triple[][][] back) {
        for(int i=0; i<back.length; i++) {
            for(int j=0; j<back[i].length; j++) {
                for(int k=0; k<back[i][j].length; k++) {
                    if(back[i][j][k] != null)
                            System.out.print(back[i][j][k].toString() + " ");
                }
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println("----------------------------------------");
        for(int i=0; i<back.length; i++) {
            for(int j=0; j<back[i].length; j++) {
                for(int k=0; k<back[i][j].length; k++) 
                    if(score[i][j][k] != 0)
                    System.out.println(score[i][j][k] + " ");
                System.out.print(" ");
            }
            System.out.println();
        }
    }
}